let accounts = [
    {
        firstName: 'Jean',
        lastName: 'Waters',
        number: 'LT758818991',
        balance: 2110,
        gender: 'M'
    },
    {
        firstName: 'Randy',
        lastName: 'Mcguire',
        number: 'LV656129320',
        balance: 1276,
        gender: 'M'
    },
    {
        firstName: 'Terrell',
        lastName: 'Nunez',
        number: 'EE198616233',
        balance: 449,
        gender: 'M'
    },
    {
        firstName: 'Katherine',
        lastName: 'Murphy',
        number: 'LT937416401',
        balance: 782,
        gender: 'F'
    },
    {
        firstName: 'Pete',
        lastName: 'Massey',
        number: 'EE318129395',
        balance: 6,
        gender: 'M'
    },
    {
        firstName: 'Audrey',
        lastName: 'Vega',
        number: 'EE352066473',
        balance: 2532,
        gender: 'F'
    },
    {
        firstName: 'Ricardo',
        lastName: 'Terry',
        number: 'EE154770478',
        balance: 2305,
        gender: 'M'
    },
    {
        firstName: 'Clark',
        lastName: 'Elliott',
        number: 'LV401842066',
        balance: 1568,
        gender: 'M'
    },
    {
        firstName: 'Kimberly',
        lastName: 'Carson',
        number: 'EE321811857',
        balance: 657,
        gender: 'F'
    },
    {
        firstName: 'Thomas',
        lastName: 'Castillo',
        number: 'EE249299846',
        balance: 842,
        gender: 'M'
    },
    {
        firstName: 'Carmen',
        lastName: 'Mitchell',
        number: 'LT780141394',
        balance: 1305,
        gender: 'F'
    },
    {
        firstName: 'Sylvester',
        lastName: 'Allen',
        number: 'LV561676248',
        balance: 1728,
        gender: 'M'
    },
    {
        firstName: 'Stephanie',
        lastName: 'Mccarthy',
        number: 'EE359902143',
        balance: 308,
        gender: 'F'
    },
    {
        firstName: 'Elijah',
        lastName: 'Munoz',
        number: 'LV447912493',
        balance: 28,
        gender: 'M'
    },
    {
        firstName: 'Daryl',
        lastName: 'Davis',
        number: '653797240',
        balance: 2500,
        gender: 'M'
    },
    {
        firstName: 'Shirley',
        lastName: 'Barber',
        number: 'LT899929247',
        balance: 1394,
        gender: 'F'
    },
    {
        firstName: 'Nichole',
        lastName: 'Ballard',
        number: 'EE384545168',
        balance: 885,
        gender: 'F'
    },
    {
        firstName: 'Guadalupe',
        lastName: 'Newton',
        number: 'LV427037539',
        balance: 465,
        gender: 'F'
    },
    {
        firstName: 'Velma',
        lastName: 'Malone',
        number: 'LT850960217',
        balance: 843,
        gender: 'F'
    },
    {
        firstName: 'Mattie',
        lastName: 'Shelton',
        number: 'LT816140665',
        balance: 1616,
        gender: 'M'
    },
    {
        firstName: 'Kerry',
        lastName: 'Parks',
        number: 'LV681403764',
        balance: 2048,
        gender: 'F'
    },
    {
        firstName: 'Ismael',
        lastName: 'Allison',
        number: 'LV470633858',
        balance: 2262,
        gender: 'M'
    },
    {
        firstName: 'Ora',
        lastName: 'Harvey',
        number: 'LV569585389',
        balance: 1391,
        gender: 'M'
    },
    {
        firstName: 'Angelina',
        lastName: 'Curtis',
        number: 'EE251446912',
        balance: 439,
        gender: 'F'
    },
    {
        firstName: 'Jennie',
        lastName: 'Myers',
        number: 'EE298765038',
        balance: 335,
        gender: 'F'
    }
];